$(document).ready(function() {
$('#slides').superslides({
    animation:"fade",
    pagination:false
});
  $('.owl-carousel').owlCarousel({
    loop:true,
    
      items:4,
    responsive:{
        0:{
            items:1
        },
        480:{
            items:2
        },
       768:{
            items:3
        },
        900:{
            items:4
        }
    }
});
   
         var skillstoppage  = $(".cv-skills").offset().top;
         $(window).scroll(function(){
             if(window.pageYOffset > skillstoppage - $(window).height() +200)
                 {
            $('.chart').easyPieChart({
            easing:'easeInOut',
            barColor:'#fff',
            trackColor:false,
            scaleColor:false,
            lineWidth:4,
            size:152,
            onStep:function(from,to,percent)
            {
                $(this.el).find('.percent').text(Math.round(percent));
            }
        }); 
                 }
         });
    $("[data-fancybox]").fancybox();
    
    $(".item").isotope({
        filter: "*",
        animationOptions:{
            duration:1500,
            easing:'linear',
            queue:false
            
        }
    });
    $(".filters a").click(function(){
        $(".filters .curent").removeClass("curent");
        $(this).addClass("curent");
        var selector=$(this).attr("data-filter");
         $(".items").isotope({
        filter: selector,
        animationOptions:{
            duration:1500,
            easing:'linear',
            queue:false
            
        }
    });
        return false;
    });
    
   
});